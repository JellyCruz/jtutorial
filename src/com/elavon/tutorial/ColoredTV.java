package com.elavon.tutorial;

public class ColoredTV extends Television{
	protected int brightness;
	protected int contrast;
	protected int picture;
	
	ColoredTV(String brand, String model){
		super(brand, model);
		brightness = 50;
		contrast = 50;
		picture = 50;
	}
	public void brightnessUp(){
		brightness++;
	}
	public void brightnessDown(){
		brightness--;
	}
	public void contrastUp(){
		contrast++;
	}
	public void contrastDown(){
		contrast--;
	}
	public void pictureUp(){
		picture++;
	}
	public void pictureDown(){
		picture--;
	}
	public void switchToChanne(int channel){
		super.channel = channel;
	}
	public void mute(){
		super.volume = 0;
	}
	public String toString(){
		return(brand + " " + model + " [on:" + powerOn + ", channel:" + channel + ", volume:" + volume + "] [b:" + brightness + ", c:" + contrast + ", p:" + picture + "]");
	}
	
	public static void main(String []args){
		Television bnwTV, sonyTV;
		bnwTV = new Television("Admiral", "A1");
		sonyTV = new ColoredTV("SONY", "S1");
		System.out.println(bnwTV.toString());
		System.out.println(sonyTV.toString());
		ColoredTV sharpTV = new ColoredTV("SHARP", "SH1");
		sharpTV.mute();
		for(int i = 0; i < 10; i++){
			sharpTV.brightnessUp();
		}
		sharpTV.brightnessDown();
		for(int i = 0; i < 20; i++){
			sharpTV.contrastUp();
		}
		sharpTV.contrastDown();
		for(int i = 0; i < 30; i++){
			sharpTV.pictureUp();
		}
		sharpTV.pictureDown();
		System.out.println(sharpTV.toString());
	}
}
