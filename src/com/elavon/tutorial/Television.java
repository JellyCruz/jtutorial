package com.elavon.tutorial;

public class Television {
		
		protected String brand;
		protected String model;
		protected boolean powerOn;
		protected int channel;
		protected int volume;
		
		Television(String brand, String model) {
		this.brand = brand;
		this.model = model;
		powerOn = false;
		channel = 0;
		volume = 5;
		}
		
		public void turnOn(){
			powerOn = true;
			}
		public void turnOff(){
			powerOn = true;
			}
		public void channelUp(){
			channel++;
			if (channel > 13){
				channel = 0;
			}
			}
		public void channelDown(){
			channel--;
			if (channel < 0){
				channel = 13;
			}
			}
		public void volumeUp(){
			if (volume < 10){
				volume++;
			}
			}
		public void volumeDown(){
			if (volume > 0){
				volume--;
			}
			}
		public String toString() {
			return(brand + " " + model + " [on:" + powerOn + ", channel:" + channel + ", volume:" + volume + "]");
		}
		public static void main(String []args){
			Television tv = new Television("Andre Electronics", "ONE");
			System.out.println(tv.toString());
			tv.turnOn();
			for(int i = 0; i < 5; i++) {
				tv.channelUp();
			}
			tv.channelDown();
			for(int i = 0; i < 3; i++) {
				tv.volumeDown();
		}
			tv.volumeUp();
			tv.turnOff();
			System.out.println(tv.toString());
		}
	
	}
