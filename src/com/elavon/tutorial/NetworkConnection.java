package com.elavon.tutorial;

public interface NetworkConnection {
	public NetworkConnection connect(String name);
	public boolean connectionStatus();
}
