package com.elavon.tutorial;

public class HelloName {

	public static void main(String[] args) {
		// exercise1 
		System.out.println(" ** *** *   *   * * ");
		System.out.println("  * *   *   *   * * ");
		System.out.println("  * **  *   *    *  ");
		System.out.println("* * *   *   *    *  ");
		System.out.println(" *  *** *** ***  *  ");
		System.out.println("");
		System.out.println("* *  *  **   *  ***  *  ");
		System.out.println("*** * * * * * *  *  * * ");
		System.out.println("*** * * **  * *  *  *** ");
		System.out.println("* * * * * * * *  *  * * ");
		System.out.println("* *  *  * *  *   *  * * ");
		System.out.println("");
		System.out.println(" ** **  * * *** ");
		System.out.println("*   * * * *   * ");
		System.out.println("*   **  * *  *  ");
		System.out.println("*   * * * * *   ");
		System.out.println(" ** * * *** *** ");
	}

}

